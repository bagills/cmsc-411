	.file	"matmul2.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"starting multiply"
.LC3:
	.string	"a result %g \n"
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB11:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$80000, %eax
	movl	$.LC0, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	subq	%rax, %rsp
	leaq	7(%rsp), %rdx
	subq	%rax, %rsp
	shrq	$3, %rdx
	leaq	7(%rsp), %r15
	subq	%rax, %rsp
	leaq	0(,%rdx,8), %r13
	movq	%rdx, -56(%rbp)
	shrq	$3, %r15
	leaq	7(%rsp), %r14
	call	puts
	movq	-56(%rbp), %rdx
	movq	.LC1(%rip), %rax
	shrq	$3, %r14
	leaq	0(,%r15,8), %r12
	leaq	0(,%r14,8), %rbx
	xorl	%edi, %edi
	movq	%rax, 808(,%rdx,8)
	movq	.LC2(%rip), %rax
	movq	%rax, 808(,%r15,8)
.L2:
	leaq	0(%r13,%rdi), %rsi
	leaq	(%r12,%rdi), %rcx
	xorl	%eax, %eax
	leaq	(%rbx,%rdi), %rdx
	.p2align 4,,10
	.p2align 3
.L3:
	movupd	(%rsi,%rax), %xmm0
	movupd	(%rcx,%rax), %xmm1
	mulpd	%xmm1, %xmm0
	movups	%xmm0, (%rdx,%rax)
	addq	$16, %rax
	cmpq	$800, %rax
	jne	.L3
	addq	$800, %rdi
	cmpq	$80000, %rdi
	jne	.L2
	movsd	808(,%r14,8), %xmm0
	movl	$.LC3, %edi
	movl	$1, %eax
	call	printf
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11:
	.size	main, .-main
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1074528256
	.align 8
.LC2:
	.long	858993459
	.long	1072902963
	.ident	"GCC: (GNU) 8.2.1 20181215 (Red Hat 8.2.1-6)"
	.section	.note.GNU-stack,"",@progbits
